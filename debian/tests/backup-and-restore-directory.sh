#!/bin/bash

ARCHIVE=/tmp/archive.fsa
MD5SUM=/tmp/md5sums

# Cleanup
function finish {
	echo "Cleaning up..."
	rm -f $ARCHIVE
	rm -rf /data
	rm -rf /restore
	rm -f $MD5SUM
}
trap finish EXIT

# Create some test data
mkdir /data
cd /data
mkdir testdir
dd if=/dev/urandom of=random bs=1M count=1 status=none
md5sum random > $MD5SUM
# hard and soft links
ln -sf random soft
ln -f random hard
# test extended attributes
touch xattr_test
xattr -w user.testkey testvalue xattr_test
# test file system ACLs
touch acl_test
setfacl -m u:nobody:r acl_test

fsarchiver savedir $ARCHIVE /data

mkdir /restore
fsarchiver restdir $ARCHIVE /restore
cd /restore/data


md5sum --quiet -c $MD5SUM

inode1=$(ls -i random | awk '{print $1}')
inode2=$(ls -i hard | awk '{print $1}')

if [ $inode1 -ne $inode2 ] ; then
	echo "hard link not properly restored, inodes differ"
	exit 1
fi

if [ "$(file soft)" != "soft: symbolic link to random" ] ; then
	echo "soft link not properly restored"
	exit 1
fi

if [ "$(xattr -p user.testkey xattr_test)" != "testvalue" ] ; then
	echo "extended attributs not properly restored"
	exit 1
fi

if ! getfacl acl_test | grep -q "user:nobody:r--" ; then
	echo "file system ACLs not properly restored"
	exit 1
fi

echo "*** Data successfully restored"

